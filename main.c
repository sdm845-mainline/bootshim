#include "types.h"


#define FB_WIDTH 1080
#define FB_HEIGHT 2160
#define FB_DEPTH 4
#define FBMEM_SIZE (FB_WIDTH * FB_DEPTH * FB_HEIGHT)
#define FBMEM(x, y) (*((volatile uint32_t*)(0x9d400000 \
											+ y * FB_WIDTH * FB_DEPTH \
											+ x * FB_DEPTH)))

void __spin_forever();

uint8_t __mpidr_el1() {
	uint32_t res = 0;

	__asm volatile
	(
		"MRS %[result], MPIDR_EL1": [result] "=r" (res)
	);
	return res;
}

uint8_t __cpu_get_id() {
	return (__mpidr_el1() >> 8) & 0xFF;
}

void clearRegion(void* region, uint32_t size) {
	uint8_t  *ptr;

	ptr = region;
	while (size--) {
		*(ptr++) = 0;
	}
}

void __early_entry() {
	// if (__cpu_get_id() > 0)
	// 	__spin_forever();

	clearRegion((void*)FBMEM(0, 0), FBMEM_SIZE);

	for (uint32_t i = 0; i < 4096; i)
	{
		uint8_t a = 255;
		uint8_t r = 255-i*2;
		uint8_t g = 0;
		uint8_t b = i*2;

		FBMEM(50 + i, 100) = (a << 24) + (r << 16) + (g << 8) + b;
	}

	__spin_forever();
}