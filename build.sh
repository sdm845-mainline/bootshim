#!/bin/bash

CROSS_COMPILE=aarch64-linux-gnu-

set -e
eval ${CROSS_COMPILE}as -mcpu=cortex-a55 -g startup.S -o startup.o
eval ${CROSS_COMPILE}gcc -c -mcpu=cortex-a55 -g main.c -o main.o
eval ${CROSS_COMPILE}ld -T link.64k.ld main.o startup.o -o boot.elf
eval ${CROSS_COMPILE}objcopy -O binary boot.elf boot.bin